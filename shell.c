/*
	Projekt 2 do predmetu POS
	Jednodychy shell
	Klara Mihalikova
	xmihal05
*/

#include <stdio.h>
#include <pthread.h>
#include <string.h>      
#include <stdlib.h>
#include <errno.h>  
#include <sys/types.h>  
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>

#define TRUE 1
#define FALSE 0
#define bool int

#define STDOUT_NO 1
#define STDIN_NO 0

#define BUF_SIZE 512	//povolenych znakov na vstupe
char *shared_buffer;	//zdielany buffer predanavy medzi mutexmi

pthread_mutex_t lock= PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

#define _POSIX_SOURCE
#define _POSIX_C_SOURCE 199506L

bool string_read=FALSE;
bool not_end = TRUE;
int backrunIterator = 1;
extern char **environ;	//pouzite pri execvp() 

#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED 1 /* XPG 4.2 - needed for WCOREDUMP() */
const char *dumpConst = "";

void printExitInfo(pid_t pid, int status){
	//vypise dovod ukoncenia child procesu
	printf("Proccess exited (pid = %d):", (int)pid); /* and one line from */
	if(WIFEXITED(status))	
		printf("	normal termination (exit code = %d)\n", WEXITSTATUS(status)); /* or */
	else if(WIFSIGNALED(status))	
		printf("	signal termination %s(signal = %d)\n", dumpConst, WTERMSIG(status)); /* or */
	else printf("	unknown type of termination\n");
}

void handler(int sig){
	//spracovanie prijateho signalu
	int ret_status;
	pid_t pid = wait(&ret_status);
	printExitInfo(pid, ret_status);
}

/****************************************************************************
************************	READ THREAD pom. funkcie	*********************
****************************************************************************/
char *getShellLine(){
	//nacitanie prikazoveho riadku
	int count = 0;	//pozicia v bufferi
	char *localBuffer = NULL;
	int myBufSize = BUF_SIZE;
	localBuffer = realloc(localBuffer, sizeof(char) * BUF_SIZE);
	if(!localBuffer){
		//zlyhala alokacia pamate buffera
		perror("Buffer allocation error\n");
		exit(EXIT_FAILURE);
	}

	while(TRUE){
		int c = getchar();

		if(c == EOF || c == '\n'){
			//skontrolujeme ci nebol odoslany prazdny riadok
			if(count == 0)
				return "";
		
			//inak pridame null znak do bufferu a odosleme ho
			localBuffer[count] = '\0';
			return localBuffer;
		}
		else{
			if(c == '<'){
				//vlozim medzeru pred
				localBuffer[count] = ' ';
				count++;	//posuniem iterator
				myBufSize++; //kvoli pridanej medzere zvacsim velkost buffera
			}

			localBuffer[count] = c;	 

			if(c == '>'){
				//vlozim medzeru za
				localBuffer[++count] = ' ';
				myBufSize++;
			}
		} 

		count++;	//inkrementujem pocitadlo

		if(count > myBufSize){
			//vypis chyby, riadok nesmie mat viac ako 512 znakov
			errno = -1;
			perror("Too many characters on input! Please be aware that input can contain of only 512 characters\n");
			exit(EXIT_FAILURE);
		}
	}
}

/****************************************************************************
******************************	READ THREAD 	*****************************
****************************************************************************/
void * readThread(){
    while(not_end){
        while(string_read);
        pthread_mutex_lock(&lock);
        printf("> ");
        shared_buffer = NULL;
        shared_buffer = getShellLine();
        if(strcmp(shared_buffer, "") == 0)
        	string_read=FALSE;
        else string_read=TRUE;
        pthread_mutex_unlock(&lock);
        pthread_cond_signal(&cond);
    }
    return NULL;
}

bool isBackRun(char **args){
	int i = 0;
	bool ret = FALSE;

	while(args[i] != NULL){
		if(strcmp(args[i], "&") == 0){
			//nahradim & poslednym znakom
			args[i] = '\0';
			ret = TRUE;
			break;
		}
		i++;
	}

	return ret;
}

char *getOutFileName(char **args){
	//ziskam nazov suboru pre zapis
	int i = 0;

	while(args[i] != '\0'){
		if(strcmp(args[i], ">") == 0){
			args[i] = '\0';
			return args[++i];
		}

		i++;	//inkrementujem pocitadlo
	}
	return "";
}

char *getInFileName(char **args){
	//ziskam vstupny subor
	int i = 0;
	while(args[i] != '\0'){
		if(strcmp(args[i], "<") == 0){
			args[i] = '\0';
			return args[++i];
		}
		i++;	//inkrementacia pocitadla
	}
	return "";
}

void my_exec(char **args, bool backrun, char *outFile, char *inFile){
	//spusti program
	pid_t pid;
	int outFd, inFd;
	int out_pipe[2];
	bool in = FALSE, out = FALSE;
	char tmpOut[4096] = {'\0'};

	//otvorim potrebne subory
	if(strcmp(outFile, "") != 0){
		//otvorim vystupny subor
		if((outFd = open(outFile, O_CREAT | O_WRONLY)) == -1){
			perror("Opening output file has failed");
			exit(EXIT_FAILURE);
		}
		out = TRUE;
		//vytvorim pipe pre prepojenie vystupu
		if(pipe(out_pipe) == -1){
			perror("Error on creating pipe for stdout");
			exit(EXIT_FAILURE);
		}
	}	
	if(strcmp(inFile, "") != 0){
		//otvorim vystupny subor
		if((inFd = open(inFile, O_RDWR)) == -1){
			perror("Opening input file has failed");
			exit(EXIT_FAILURE);
		}
		//presmerujem vstup
		in = TRUE;
	}

	signal(SIGCHLD, handler);
	pid = fork();	//vytvorim novy proces
	if(pid == 0){
	//child proces nahradim programom		
		if(out){
			//presmerujem vystup do pipe
			close(out_pipe[0]);
			dup2(out_pipe[1], STDOUT_NO);
			close(out_pipe[1]);
		}
		if(in)
			//nahradin STDIN suborom
			dup2(inFd, STDIN_FILENO);
		if(execvp(args[0], &args[0]) == -1){
			perror("Error on program execution");
			_exit(EXIT_FAILURE);
		}
	}
	else if(pid == -1){
		perror("fork() failed");
		_exit(EXIT_FAILURE);
	}
	else{
		if(out){
			//nacitanie vystupnych dat od childa
			close(out_pipe[1]);
			//nacitanie dat
			int ret;
			while((ret = read(out_pipe[0], tmpOut, sizeof(tmpOut))) > 0){
				//zapis do suboru
				if(write(outFd, &tmpOut, sizeof(tmpOut)) < 0){
					perror("Writting redirected stdout into file failed");
					exit(EXIT_FAILURE);				
				}
			}
			if(ret == -1){
				perror("Reading redirected stdout failed");
				exit(EXIT_FAILURE);
			}	
			//uzavretie suboru
			close(outFd);
		}		
		if(in)
			close(inFd);
		if(!backrun) 
			sleep(2);
			
	}
}

void parseInput(char **args){

	//zistim ci budem spustat na pozadi
	bool back = isBackRun(args);
	//ziskam subor na zapis
	char *outFileName =	getOutFileName(args);
	//ziskam vstupny subor
	char *inFileName = getInFileName(args);	

	//execute
	my_exec(args, back, outFileName, inFileName);
}

void execute_cd(char **args){
	//funkcia nahradzajuca cd..
	if(args[1] == NULL){
		errno = EINVAL;
		perror("cd expects argument");
		//neukoncujem program, iba vypisem chybu
	}
	else{
		//zmenim directory
		if(chdir(args[1]) != 0){
			perror("Directory change (cd) failed");
		}
	}
}

char **splitArgs(){
	//rozparsovanie jednotlivych argumentov
	char **tokens = NULL;
	char *token = strtok(shared_buffer, " ");
	int n_spaces = 0;

	while(token){
		//realokacia pamate
		tokens = realloc(tokens, sizeof(char *) * ++n_spaces);
		if(tokens == NULL){
			//chyba realokacie
			perror("Memory reallocation failed");
			exit(EXIT_FAILURE);
		}

		tokens[n_spaces-1] = token;
		token = strtok(NULL, " ");
	}
	tokens = realloc(tokens, sizeof(char *) * ++n_spaces);
	tokens[n_spaces-1] = '\0';
	return tokens; //vratim rozparsovane argumenty 
}

/****************************************************************************
**********************	EXECUTE THREAD pom.funkcie	*************************
****************************************************************************/
void * executeThread(){
	char **args = (char **)malloc(sizeof(char **));;	//k rozprarsovaniu lvstupu na argumenty
    
    while(not_end){
        args = realloc(args, sizeof(char **));
        pthread_mutex_lock(&lock);
        while(!string_read){
            pthread_cond_wait(&cond,&lock);
        }
        args = splitArgs();		//rozparsovanie argumentov
			
		if(strcmp(args[0], "exit") == 0){
			not_end = FALSE;
			printf("Shell terminated with exit code: 0\n");
			exit(EXIT_SUCCESS);
			break;
		}
		else if(strcmp(args[0], "cd") == 0)
			execute_cd(args);
		else parseInput(args);	//hladanie presmerovania >,< alebo &

        string_read=FALSE;
        pthread_mutex_unlock(&lock);
    }
    return NULL;
}

/****************************************************************************
****************************	MAIN	************************************
****************************************************************************/
int main(int argc, char *argv[], char **envp)
{
	//ulozenie PATH do environ
	int i = 0;
	while(envp[i] != NULL){
		char *substr = malloc(sizeof(char));
		strncpy(substr, envp[i], 5);
		if(strcmp(substr, "PATH=") == 0){
			strcpy(environ[0], envp[i]);
			environ[1] = NULL;
			break;
		}
		i++;
	}

	//WCOREDUMP() nemusi byt dostupne na vsetkych systemoch
	#ifdef COREDUMP
		strcpy(dumpConst, "with core dump ");
	#endif

    pthread_t tr, te;
    shared_buffer = malloc(sizeof(char) * BUF_SIZE);

	//vypisem WELCOME MESSAGE
	printf("###################################################\n"); 
	printf("###		Welcome to Shell		###\n"); 
	printf("###################################################\n");

    //vytvorenie vlaken
    if(pthread_create(&tr,NULL,readThread,NULL) != 0){
		perror("Read thread could not be created");
		exit(EXIT_FAILURE);    	
    }
    if(pthread_create(&te,NULL,executeThread,NULL) != 0){
		perror("Read thread could not be created");
		exit(EXIT_FAILURE);    	
    }
    //zlucenie vlaken
    pthread_join(tr,NULL);
    pthread_join(te,NULL);
    exit(EXIT_SUCCESS);
}